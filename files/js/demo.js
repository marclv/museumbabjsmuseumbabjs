﻿'use strict';

var oldDoigtX=0;
var differenceDoigt=0.0;
var numeroEtape=0;
var deplacementVersEtape="";
var opacitePresentationOeuvre=0;
var opaciteAReduire=0;
var metzingerAChanger=0;
function runDemo(canvasId) {
     var canvas = document.getElementById(canvasId);
     var engine = new BABYLON.Engine(canvas, true);

     // Création de la scène
     var scene = new BABYLON.Scene(engine);

    // Ajout d'une caméra et de son contrôleur
	var camera = new BABYLON.FreeCamera("MainCamera", new BABYLON.Vector3(9.611, 2.7, -18.68), scene);
	camera.speed = 0.25;
	var vitesseDeplacementsAuto=0.05;
	camera.rotation = new BABYLON.Vector3(-0.07208661257251964,0.9172761720586663,0);
	//Set the ellipsoid around the camera (e.g. your player's size)
	camera.ellipsoid = new BABYLON.Vector3(1, 1.35, 1);
	camera.applyGravity = true;
	camera.checkCollisions = true;
	camera.fov=1.25;
	scene.activeCamera.attachControl(canvas);

	
	var light2 = new BABYLON.DirectionalLight("dir01", new BABYLON.Vector3(-0.3, -0.7, 0), scene);
	light2.position = new BABYLON.Vector3(100, 820, 0);
	light2.specular = new BABYLON.Color3(0, 0, 0);
	light2.diffuse = new BABYLON.Color3(1, 1, 1);
	light2.intensity = 1;
	
	
	var hemiLumiere = new BABYLON.HemisphericLight("Hemi0", new BABYLON.Vector3(0.3, 0.2, 0), scene);
	hemiLumiere.diffuse = new BABYLON.Color3(1.01, 0.975, 0.975);
	hemiLumiere.specular = new BABYLON.Color3(0, 0, 0);
	hemiLumiere.intensity = Math.abs(2.6+light2.direction.y)/2; //attention changé aussi avec le slider, monter le chiffre avant + pour plus de lum
	// hemiLumiere.intensity = 0.8;
	// hemiLumiere.intensity = 100;
	hemiLumiere.groundColor = new BABYLON.Color3(0.41, 0.405, 0.4);	
	// var postProcess = new BABYLON.ColorCorrectionPostProcess("color_correction", "./table.png", 5, camera, null, engine, true);
	// var blurWidth = 1.0;

// var postProcess0 = new BABYLON.PassPostProcess("Scene copy", 1.0, camera);
// var postProcess1 = new BABYLON.PostProcess("Down sample", "./Scenes/Customs/postprocesses/downsample", ["screenSize", "highlightThreshold"], null, 0.25, camera, BABYLON.Texture.BILINEAR_SAMPLINGMODE);
// postProcess1.onApply = function (effect) {
    // effect.setFloat2("screenSize", postProcess1.width, postProcess1.height);
    // effect.setFloat("highlightThreshold", 0.90);
// };
// var postProcess2 = new BABYLON.BlurPostProcess("Horizontal blur", new BABYLON.Vector2(1.0, 0), blurWidth, 0.25, camera);
// var postProcess3 = new BABYLON.BlurPostProcess("Vertical blur", new BABYLON.Vector2(0, 1.0), blurWidth, 0.25, camera);
// var postProcess4 = new BABYLON.PostProcess("Final compose", "./Scenes/Customs/postprocesses/compose", ["sceneIntensity", "glowIntensity", "highlightIntensity"], ["sceneSampler"], 1, camera);
// postProcess4.onApply = function (effect) {
    // effect.setTextureFromPostProcess("sceneSampler", postProcess0);
    // effect.setFloat("sceneIntensity", 0.5);
    // effect.setFloat("glowIntensity", 0.4);
    // effect.setFloat("highlightIntensity", 1.0);
// };


//ON VIRE LE EMIT DES MURS
//ON DONNE UN EMIT A LA SKYBOX
//ON PEUT AVOIR UN JOLI EFFET HDR je pense

	// HDR Test
	// var hdr = new BABYLON.HDRRenderingPipeline("hdr", scene, 1.0, null, camera);
	// hdr.brightThreshold = 0.7; // Minimum luminance needed to compute HDR
	// hdr.gaussCoeff = 0.5; // Gaussian coefficient = gaussCoeff * theEffectOutput;
	// hdr.gaussMean = 1; // The Gaussian blur mean
	// hdr.gaussStandDev = 5; // Standard Deviation of the gaussian blur.
	// hdr.exposure = 1.0; // Controls the overall intensity of the pipeline
	// hdr.minimumLuminance = 0.2; // Minimum luminance that the post-process can output. Luminance is >= 0
	// hdr.maximumLuminance = 0.3; //Maximum luminance that the post-process can output. Must be suprerior to minimumLuminance 
	// hdr.luminanceDecreaseRate = 0.3; // Decrease rate: dark to white
	// hdr.luminanceIncreaserate = 0.5; // Increase rate: white to dark
	// var antialiasing = new BABYLON.FxaaPostProcess("fxaa", 1.0, camera);
	
	// Create the "God Rays" effect (volumetric light scattering)
	// var godrays = new BABYLON.VolumetricLightScatteringPostProcess('godrays', 1.0, camera, null, 100, BABYLON.Texture.BILINEAR_SAMPLINGMODE, engine, false);

	// By default it uses a billboard to render the sun, just apply the desired texture
	// position and scale
	// godrays.mesh.material.diffuseTexture = new BABYLON.Texture('img/sun.png', scene, true, false, BABYLON.Texture.BILINEAR_SAMPLINGMODE);
	// godrays.mesh.material.diffuseTexture.hasAlpha = true;
	// godrays.mesh.position = new BABYLON.Vector3(100, 50, 0);
	// godrays.mesh.scaling = new BABYLON.Vector3(20, 20, 20);
	// godrays.exposure = 0.1;

	var etapes=[];
	etapes[0]=[17.3,2.71,-8.38]; //devant banc
	etapes[1]=[20.30,2.71,-2.28]; //devant esquisses picasso
	etapes[2]=[18.2,2.725,9.82]; //devant metzinger
	etapes[3]=[19.1,2.72,22.03]; //devant nature morte durin
	etapes[4]=[18.9,2.705,35.15]; //devant nature morte cubique herbin et tete braque
	etapes[5]=[13.7,2.71,44.7]; //devant otto
	etapes[6]=[-5.5,2.7,44.3]; //devant la statue
	etapes[7]=[-5.70,2.72,-4.47]; //devant le Delaunay Rythme2
	
	
	var cubeEtapes = BABYLON.Mesh.CreateBox("box", 1, scene);
	cubeEtapes.position=new BABYLON.Vector3(etapes[0][0],etapes[0][1],etapes[0][2]);
	cubeEtapes.visibility=0;
	camera.setTarget(cubeEtapes.position);
	
	// var cubeBoussole = BABYLON.Mesh.CreateBox("box", 0.2, scene); //pointe vers le waypoint
	// cubeBoussole.scaling.z=3;
	// cubeBoussole.position=new BABYLON.Vector3(camera.position.x, 4, camera.position.z);
	
	
	// spot pour tests
	// var spotMisEnPlace = new BABYLON.SpotLight("spotMisEnPlace", new BABYLON.Vector3(16, 2.8, -1.98), new BABYLON.Vector3(-200, -239, -3), 750, 1, scene);
	// spotMisEnPlace.diffuse = new BABYLON.Color3(1, 3, 1);
	// spotMisEnPlace.specular = new BABYLON.Color3(1.2, 1, 1);
	// spotMisEnPlace.intensity = 0;
	
	// spot pour piliers et plafond
	var spotPiliers = new BABYLON.SpotLight("spotPiliers", new BABYLON.Vector3(16, 2.8, -1.98), new BABYLON.Vector3(-200, -239, -3), 700, 1, scene);
	spotPiliers.diffuse = new BABYLON.Color3(0.95, 1.05, 1.15);
	spotPiliers.specular = new BABYLON.Color3(1, 1.1, 1.1);
	spotPiliers.intensity = 0.5;
	
	
	// spotFruits
	var spotFruits = new BABYLON.SpotLight("spotFruits", new BABYLON.Vector3(20, 20, -6), new BABYLON.Vector3(-94, -50, -6), 1.5, 30, scene);
	spotFruits.diffuse = new BABYLON.Color3(1, 1, 1);
	spotFruits.specular = new BABYLON.Color3(1.2, 1, 1);
	spotFruits.intensity = 0.3;


	// spotDelaunayEquipeCardiff
	var spotDelaunayEquipeCardiff = new BABYLON.SpotLight("spotDelaunayEquipeCardiff", new BABYLON.Vector3(11.1, 20, 16), new BABYLON.Vector3(-87, -47, 9), 160.8, 30, scene);
	// var cubeGizmo = BABYLON.Mesh.CreateBox("box", 1, scene);
	// cubeGizmo.position=new BABYLON.Vector3(9.75, 15, 35.5);
	spotDelaunayEquipeCardiff.diffuse = new BABYLON.Color3(1, 1, 1);
	spotDelaunayEquipeCardiff.specular = new BABYLON.Color3(1.2, 1, 1);
	spotDelaunayEquipeCardiff.intensity = 0.5;

	//spotGleizesBaigneuses
	var spotGleizesBaigneuses = new BABYLON.SpotLight("spotGleizesBaigneuses", new BABYLON.Vector3(9.75, 20, 30), new BABYLON.Vector3(-73, -60, 9), 518, 30, scene);
	spotGleizesBaigneuses.diffuse = new BABYLON.Color3(1, 1, 1);
	spotGleizesBaigneuses.specular = new BABYLON.Color3(1.2, 1, 1);
	spotGleizesBaigneuses.intensity = 0.5;
	
	//spotSurOttoStolp
	var spotSurOttoStolp = new BABYLON.SpotLight("spotSurOttoStolp", new BABYLON.Vector3(9.75, 25, 30), new BABYLON.Vector3(15, -85, 82), 825, 30, scene);
	spotSurOttoStolp.diffuse = new BABYLON.Color3(1, 1, 1);
	spotSurOttoStolp.specular = new BABYLON.Color3(1.2, 1, 1);
	spotSurOttoStolp.intensity = 0.6;
	
	// spotOttoStolp
	var spotOttoStolp = new BABYLON.SpotLight("	spotOttoStolp.position=new BABYLON.Vector3(9.75, 15, 35.5);", new BABYLON.Vector3(9.75, 15, 35.5), new BABYLON.Vector3(20, -53, 82), -21.9, 165, scene);
	spotOttoStolp.position=new BABYLON.Vector3(9.75, 15, 35.5);
	spotOttoStolp.diffuse = new BABYLON.Color3(1.1, 1, 0.9);
	var SpotOttoStolpCouleurR=0.8;
	spotOttoStolp.specular = new BABYLON.Color3(1.2, 1, 1);
	spotOttoStolp.intensity = 1.2;
	
	// spotBraqueTeteDeFemme
	var spotBraqueTeteDeFemme = new BABYLON.SpotLight("spotBraqueTeteDeFemme", new BABYLON.Vector3(5.1, 22, 35.5), new BABYLON.Vector3(225, -220, 36), 350.8, 165, scene);
	spotBraqueTeteDeFemme.diffuse = new BABYLON.Color3(1.1, 1, 0.9);
	spotBraqueTeteDeFemme.specular = new BABYLON.Color3(1.2, 1, 1);
	spotBraqueTeteDeFemme.intensity = 0.7;
	
	// spotHerbinNatureMorte
	var spotHerbinNatureMorte = new BABYLON.SpotLight("spotHerbinNatureMorte", new BABYLON.Vector3(10.5, 20, 33), new BABYLON.Vector3(225, -280, 17), 228.5, 165, scene);
	spotHerbinNatureMorte.diffuse = new BABYLON.Color3(1.1, 1, 0.9);
	spotHerbinNatureMorte.specular = new BABYLON.Color3(1.2, 1, 1);
	spotHerbinNatureMorte.intensity = 0.8;
	
	//spot derainNatureMorte
	var spotderainNatureMorte = new BABYLON.SpotLight("spotderainNatureMorte", new BABYLON.Vector3(-22, 15, 22), new BABYLON.Vector3(313, -87, 3), 581, 165, scene);
	spotderainNatureMorte.diffuse = new BABYLON.Color3(1.1, 1, 0.90);
	spotderainNatureMorte.specular = new BABYLON.Color3(1.2, 1, 1);
	spotderainNatureMorte.intensity = 0.3;
	
	// spotMetzinger
	var spotMetzinger = new BABYLON.SpotLight("spotMisEnPlace", new BABYLON.Vector3(-22, 6, 10), new BABYLON.Vector3(313, -6, 0), 450, 165, scene);
	spotMetzinger.diffuse = new BABYLON.Color3(1.3, 1, 0.9);
	spotMetzinger.specular = new BABYLON.Color3(1.2, 1, 1);
	spotMetzinger.intensity = 0.5;
	
	// spotArlequin
	var spotArlequin = new BABYLON.SpotLight("spotArlequin", new BABYLON.Vector3(12, 6, 0), new BABYLON.Vector3(200, -48, -82), 450, 165, scene);
	spotArlequin.diffuse = new BABYLON.Color3(1.3, 1, 0.9);
	spotArlequin.specular = new BABYLON.Color3(1.2, 1, 2);
	spotArlequin.intensity = 0.25;
	
	// spot pour le premier rassemblement de tableaux bordFenetre1
	var spotbordFenetre1 = new BABYLON.SpotLight("spotbordFenetre1", new BABYLON.Vector3(-10, 15, -2.5), new BABYLON.Vector3(25, -10, 0.3), 900, 400, scene);
	spotbordFenetre1.diffuse = new BABYLON.Color3(1, 1, 1);
	spotbordFenetre1.specular = new BABYLON.Color3(1.2, 1, 1);
	spotbordFenetre1.intensity = 0.8;
	
	// Création d'un spot sur le tableau de spotVanDongen
	var spotVanDongen = new BABYLON.SpotLight("spotVanDongen", new BABYLON.Vector3(12, 6, 0), new BABYLON.Vector3(200, -50, 7), 450, 165, scene);
	spotVanDongen.diffuse = new BABYLON.Color3(1.3, 1, 0.9);
	spotVanDongen.specular = new BABYLON.Color3(2.2, 1, 1);
	spotVanDongen.intensity = 0.2;

	// Création d'un spot sur le tableau de Delaunay
	var spot2 = new BABYLON.SpotLight("Spot2", new BABYLON.Vector3(10, 11, 23), new BABYLON.Vector3(14, -130, 151), 500, 10, scene);
	spot2.diffuse = new BABYLON.Color3(1.8, 1.77, 1.77);
	spot2.specular = new BABYLON.Color3(0.2, 0.18, 0.18);
	spot2.intensity = 0;	

	// Création d'un spot sur le tableau de Delaunay tour effeil
	var spot3 = new BABYLON.SpotLight("Spot3", new BABYLON.Vector3(-0.7, 10.03, 10), new BABYLON.Vector3(-77, -37, -2), 1, 1, scene);
	spot3.diffuse = new BABYLON.Color3(0.5, 0.5, 0.5);
	spot3.specular = new BABYLON.Color3(0.2, 0.18, 0.18);
	spot3.intensity = 0;


	//creation du sol
	var ground = BABYLON.Mesh.CreatePlane("ground", 170, scene);
    ground.rotation.x = Math.PI / 2;
    ground.position.y=0.05;
	light2.includedOnlyMeshes.push(ground);
	// ground.checkCollisions = true;	
	
	//creation du sol exterieur
	var solExterieur = BABYLON.Mesh.CreatePlane("solExterieur", 100, scene);
    solExterieur.rotation.x = Math.PI / 2;
    solExterieur.position.y=0.08;
	solExterieur.position.x=73.5;
	// godrays.excludedMeshes.push(solExterieur);
	// solExterieur.checkCollisions = true;	
	
	//creation du reflet
	var solPourReflets = BABYLON.Mesh.CreatePlane("solPourReflets", 170, scene);
    solPourReflets.rotation.x = Math.PI / 2;
    solPourReflets.position.y=0.05;
	solPourReflets.checkCollisions = true;
    
	//carrelage
	var carrelageMuseeMateriel = new BABYLON.StandardMaterial("gMaterial", scene);
    carrelageMuseeMateriel.diffuseTexture = new BABYLON.Texture("img/ground.png?1", scene);
	carrelageMuseeMateriel.diffuseTexture.uScale = 15;//Repeat 5 times on the Vertical Axes
    carrelageMuseeMateriel.diffuseTexture.vScale = 35;//Repeat 5 times on the Horizontal Axes	
	carrelageMuseeMateriel.specularColor = new BABYLON.Color3(1, 1, 1);
	carrelageMuseeMateriel.ambientColor  = new BABYLON.Color3(10, 10, 10);
	ground.material=carrelageMuseeMateriel;
	
	//texture sol exterieur
	var pavesExterieursMateriel = new BABYLON.StandardMaterial("herbe", scene);
	// pavesExterieursMateriel.diffuseColor = new BABYLON.Color3(0.15, 0.25, 0.15);
    pavesExterieursMateriel.diffuseTexture = new BABYLON.Texture("img/textureHerbe.jpg?2", scene);
	pavesExterieursMateriel.diffuseTexture.uScale = 28;
    pavesExterieursMateriel.diffuseTexture.vScale = 28;
	pavesExterieursMateriel.specularColor = new BABYLON.Color3(1, 1, 1);
	solExterieur.material=pavesExterieursMateriel;
	
    //Creation of a mirror material
    var mirrorMaterial = new BABYLON.StandardMaterial("texture4", scene);
    mirrorMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
    mirrorMaterial.reflectionTexture = new BABYLON.MirrorTexture("mirror", 256, scene, true); //Create a mirror texture
    mirrorMaterial.reflectionTexture.mirrorPlane = new BABYLON.Plane(0, -0.5, 0, 0);
    mirrorMaterial.reflectionTexture.level = 1;//Select the level (0.0 > 1.0) of the reflection
	mirrorMaterial.alpha=0.06;
	solPourReflets.material=mirrorMaterial;	
	
    //texture vitre
    var vitreMateriel = new BABYLON.StandardMaterial("texture4", scene);
    vitreMateriel.diffuseColor = new BABYLON.Color3(0.4, 0.4, 0.4);
    vitreMateriel.reflectionTexture = new BABYLON.MirrorTexture("mirror", 512, scene, true); //Create a mirror texture

    vitreMateriel.reflectionTexture.level = 0.6;//Select the level (0.0 > 1.0) of the reflection
	vitreMateriel.reflectionTexture.renderList.push(ground);
	vitreMateriel.alpha=0.1;	
	
	//arbres et nuage au fond
	var skybox = BABYLON.Mesh.CreateBox("skybox", 400.0, scene);
	var skyboxMaterial = new BABYLON.StandardMaterial("skybox", scene);
	skybox.rotation.y=0.9;
	skyboxMaterial.backFaceCulling = false;
	skybox.material = skyboxMaterial;
	skybox.infiniteDistance = true;
	skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
	skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
	skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("img/skybox", scene);
	skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
	skybox.renderingGroupId = 0;

	// Création d'un cube parentMusee
	var parentMusee = BABYLON.Mesh.CreateBox("box", 0.01, scene);
	parentMusee.material=new BABYLON.StandardMaterial("gMaterial", scene);
	parentMusee.material.specularColor = new BABYLON.Color3(0.3, 0.3, 0.3);

	// Création d'un cube de visite
	// var cubeVisite1 = BABYLON.Mesh.CreateBox("box", 0.5, scene);
	// cubeVisite1.material=new BABYLON.StandardMaterial("gMaterial", scene);
	// cubeVisite1.material.specularColor = new BABYLON.Color3(0.3, 0.3, 0.3);

	//ombres exterieurs
	var shadowGenerator = new BABYLON.ShadowGenerator(256, light2);
	shadowGenerator.useBlurVarianceShadowMap  = true;
	shadowGenerator.blurScale=1.2;
	ground.receiveShadows = true;
	
	//ombres spotMisEnPlace
	// var shadowGeneratorspotMisEnPlace = new BABYLON.ShadowGenerator(512, spotMisEnPlace);
	// shadowGeneratorspotMisEnPlace.useBlurVarianceShadowMap = true;

	// chargement des objets 3D
    var LIBRARY = {};
	var delaunayEffeil;
	var derainNatureMorteATable;
	var derainPort;
	var derainPort;
	var delaunayFruits;
	var ottoStolp;
	var gleizesBaigneuses;
	var ombreMetzinger;
	var metzingerLOiseauBleu;
	var picassoAuCirque;
	var pilier1;
	var pilier2;
	var pilier3;
	var pilier4;
	var chargeMusee = new BABYLON.AssetsManager(scene);
	var musee = chargeMusee.addMeshTask("museum", "", "./", "museeproportionsok.babylon?7");
	musee.onSuccess = function(task) {
		LIBRARY['musee'] = task.loadedMeshes;
	};

	chargeMusee.load();
	
	chargeMusee.onFinish = function (tasks) {
		//on fusionne tout avec parentMusee
		LIBRARY['musee'].forEach(function(entry) {
			entry.parent=parentMusee;
			entry.receiveShadows=true;
			// entry.checkCollisions = true;
			 mirrorMaterial.reflectionTexture.renderList.push(entry);
			vitreMateriel.reflectionTexture.renderList.push(entry);
			 // alert(entry.name);
			 if(entry.name=="toit" || entry.name=="bordfenetre0" || entry.name=="bordfenetre1" || entry.name=="bordfenetre2" || entry.name=="bordfenetre3"
			 || entry.name=="bordfenetre4" || entry.name=="bordfenetre5" || entry.name=="banc1" || entry.name=="banc2"
			 // || entry.name=="noirFenetre1" || entry.name=="noirFenetre2" || entry.name=="noirFenetre3" || entry.name=="noirFenetre4" || entry.name=="noirFenetre5"
			 // || entry.name=="delaunayEffeil" || entry.name=="derainNatureMorteATable" || entry.name=="fruits"
			 ){
				shadowGenerator.getShadowMap().renderList.push(entry);
			 }
			 if(entry.name=="vitre1" || entry.name=="vitre2" || entry.name=="vitre3" || entry.name=="vitre4"){
				entry.material=vitreMateriel;
				var pointsArray = [];
				var meshWorldMatrix = entry.computeWorldMatrix();
				var verticesPosition = entry.getVerticesData(BABYLON.VertexBuffer.PositionKind);
				var offset = 0
				for (var i = 0; i < 3; i++) {
					var v = entry.getIndices()[i + offset];
					pointsArray.push(BABYLON.Vector3.TransformCoordinates(BABYLON.Vector3.FromArray(verticesPosition, v*3), meshWorldMatrix));
				}
				var plane = BABYLON.Plane.FromPoints(pointsArray[0], pointsArray[1], pointsArray[2]);	
				vitreMateriel.reflectionTexture.mirrorPlane = plane;
			 }
			 
			// if(entry.name=="delaunayEffeil"){
				// spotMisEnPlace.excludedMeshes = [entry];
				// spot2.excludedMeshes = [entry];
			 // }D
			 if(entry.name=="fruits" || entry.name=="murFondSousVoute" || entry.name=="Cube.010" || entry.name=="Cube.005"
			 ){
				spotFruits.includedOnlyMeshes.push(entry);
			 }
			 if(entry.name=="spotDelaunayEquipeCardiff" || entry.name=="murFondSousVoute" || entry.name=="Cube.023" || entry.name=="Cube.012"
			 || entry.name=="Cube.015" || entry.name=="Cube.002"
			 ){
				spotSurOttoStolp.includedOnlyMeshes.push(entry);
			 }
			 if(entry.name=="spotDelaunayEquipeCardiff" || entry.name=="murFondSousVoute" || entry.name=="Cube.023" || entry.name=="Cube.012"
			 || entry.name=="Cube.015" || entry.name=="Cube.002"
			 ){
				spotOttoStolp.includedOnlyMeshes.push(entry);
			 }
			 if(entry.name=="spotDelaunayEquipeCardiff" || entry.name=="murFondSousVoute" || entry.name=="Cube.008" || entry.name=="Cube.009"
			 || entry.name=="Cube.025"
			 ){
				spotDelaunayEquipeCardiff.includedOnlyMeshes.push(entry);
			 }
			 if(entry.name=="spotGleizesBaigneuses" || entry.name=="murFondSousVoute" || entry.name=="Cube.008" || entry.name=="Cube.007"
			 || entry.name=="Cube.027"
			 ){
				spotGleizesBaigneuses.includedOnlyMeshes.push(entry);
			 }
			 if(entry.name=="bordfenetre4" || entry.name=="derainNatureMorteATable"
			 ){
				spotderainNatureMorte.includedOnlyMeshes.push(entry);
			 }
			 if(entry.name=="herbinNatureMorte" || entry.name=="braqueTeteDeFemme" || entry.name=="bordfenetre3"
			 ){
				spotBraqueTeteDeFemme.includedOnlyMeshes.push(entry);
			 }
			 if(entry.name=="herbinNatureMorte" || entry.name=="braqueTeteDeFemme" || entry.name=="bordfenetre3"
			 ){
				spotHerbinNatureMorte.includedOnlyMeshes.push(entry);
			 }
			 if(entry.name=="bordfenetre2" || entry.name=="metzingerLOiseauBleu" || entry.name=="delaunayEffeil"
			 ){
				spotMetzinger.includedOnlyMeshes.push(entry);
			 }
			 if(entry.name=="bordfenetre1" || entry.name=="picassoSaltimbanque" || entry.name=="picassoLaToilette" || entry.name=="vanDongenCorbeille"
			 ){
				spotVanDongen.includedOnlyMeshes.push(entry);
			 }
			 if(entry.name=="bordfenetre1" || entry.name=="picassoAuCirque" || entry.name=="picassoSaltimbanque" || 
			 entry.name=="picassoLaToilette" || entry.name=="vanDongenCorbeille" || entry.name=="picassoBusteDHomme" || entry.name=="picassoArlequin"
			 ){
				spotbordFenetre1.includedOnlyMeshes.push(entry);
				spotArlequin.includedOnlyMeshes.push(entry);
				// spotMisEnPlace.excludedMeshes.push(entry);
			 }
			 if(entry.name=="murFondSousVoute" || entry.name=="Cube.005" || entry.name=="rebordtoit" || entry.name=="delaunayRythme3"
			 || entry.name=="Cube.007" || entry.name=="Cube.008" || entry.name=="Cube.009" || entry.name=="Cube.010"
			 || entry.name=="Cube" || entry.name=="Cube.006" || entry.name=="Cube.001" || entry.name=="Cube.004" || entry.name=="Cube.012"
			 ){
				light2.includedOnlyMeshes.push(entry);
			 }
			 if(entry.name=="Cube.015" || entry.name=="Cube.014" || entry.name=="Cube.012"){
				spot2.includedOnlyMeshes.push(entry);
			 }
			 if(entry.name=="delaunayEffeil" || entry.name=="murFondSousVoute" || entry.name=="Cube.008"|| entry.name=="Cube.009"|| entry.name=="Cube.005"){
				spot3.includedOnlyMeshes.push(entry);
			 }
			 
			 if(entry.name=="derainNatureMorteATable" || entry.name=="derainPort"){				 
				derainNatureMorteATable=entry;
				// shadowGeneratorspotMisEnPlace.getShadowMap().renderList.push(entry);
			 }
			 if(entry.name=="picassoAuCirque"){				 
				picassoAuCirque=entry;
			 }
			 if(entry.name=="delaunayEffeil"){				 
				delaunayEffeil=entry;
			 }
			 if(entry.name=="derainPort"){				 
				derainPort=entry;
			 }
			 if(entry.name=="fruits"){				 
				delaunayFruits=entry;
			 }
			 if(entry.name=="ottoStolp"){				 
				ottoStolp=entry;
			 }
			 if(entry.name=="gleizesBaigneuses"){				 
				gleizesBaigneuses=entry;
			 }
			 if(entry.name=="ombreMetzinger"){				 
				ombreMetzinger=entry;
			 }
			 if(entry.name=="metzingerLOiseauBleu"){				 
				metzingerLOiseauBleu=entry;
			 }
			 if(entry.name=="vitre1"){				 
				// godrays.excludedMeshes.push(entry);
			 }
			 if(entry.name=="Cube.006"){				 
				pilier1=entry;				
				spotPiliers.includedOnlyMeshes.push(entry);
			 }
			 if(entry.name=="Cube.001"){				 
				pilier2=entry;				
				spotPiliers.includedOnlyMeshes.push(entry);
			 }
			 if(entry.name=="Cube.004"){				 
				pilier3=entry;				
				spotPiliers.includedOnlyMeshes.push(entry);
			 }
			 if(entry.name=="Cube"){				 
				pilier4=entry;				
				spotPiliers.includedOnlyMeshes.push(entry);
			 }
			 if(entry.name=="rebordtoit" || entry.name=="toit"){				 			
				spotPiliers.includedOnlyMeshes.push(entry);
			 }
			 
		});		

	}
	

	sliderLumiere.value=light2.direction.y*100;	
	// sliderspotMisEnPlacex.value=spotMisEnPlace.direction.x;
	// sliderspotMisEnPlacey.value=spotMisEnPlace.direction.y;
	// sliderspotMisEnPlacez.value=spotMisEnPlace.direction.z;
	// sliderspotMisEnPlacePosX.value=spotMisEnPlace.position.x;
	// sliderspotMisEnPlaceangle.value=spotMisEnPlace.angle;
	var spot2CouleurR;
	
	//juste avant le rendu
	scene.registerBeforeRender(function () {
		// light2.direction.y-=0.01;
		
		// if(light2.direction.y>-0.2){
			// hemiLumiere.intensity = 0.5;
		// }
		// if(light2.direction.y<-0.2){
			// hemiLumiere.intensity = 0.7;
		// }
		// light2.direction.y=-0.2;
		// document.getElementById("infosspotMisEnPlace").innerHTML = "x:"+sliderspotMisEnPlacex.value+",y:"+sliderspotMisEnPlacey.value+",z:"+sliderspotMisEnPlacez.value+",posx:"+sliderspotMisEnPlacePosX.value+",angle:"+sliderspotMisEnPlaceangle.value;
		if(document.getElementById("presentationOeuvre").style.display=="block" && document.getElementById("presentationOeuvre").style.opacity<1 && opaciteAReduire==0){
			 opacitePresentationOeuvre=opacitePresentationOeuvre+0.02;
			document.getElementById("presentationOeuvre").style.opacity=opacitePresentationOeuvre;
			document.getElementById("zoomOeuvre").style.opacity=opacitePresentationOeuvre;
			document.getElementById("boutonsInterfaceTableaux").style.opacity=opacitePresentationOeuvre;
		}
		
		if(opaciteAReduire==1 && document.getElementById("presentationOeuvre").style.opacity>0){
			 opacitePresentationOeuvre=opacitePresentationOeuvre-0.04;
			document.getElementById("presentationOeuvre").style.opacity=opacitePresentationOeuvre;
			document.getElementById("zoomOeuvre").style.opacity=opacitePresentationOeuvre;
			document.getElementById("boutonsInterfaceTableaux").style.opacity=opacitePresentationOeuvre;
		}
		if(opaciteAReduire==1 && document.getElementById("presentationOeuvre").style.opacity<0.1){
			opaciteAReduire=0;
			document.getElementById("presentationOeuvre").style.display="none";
			document.getElementById("zoomOeuvre").style.display="none";
			document.getElementById("boutonsInterfaceTableaux").style.display="none";			
		}
		// if(document.getElementById("presentationOeuvre").style.display=="none" && document.getElementById("presentationOeuvre").style.opacity>0){
			// document.getElementById("presentationOeuvre").style.opacity-=0.01;
		// }

		if(gleizesBaigneuses){
			// spotMisEnPlace.setDirectionToTarget(gleizesBaigneuses.position);
		}
		
		//mouvements du doigt sur un écran
		if(differenceDoigt>1){
			camera.rotation.y+=0.1;
			differenceDoigt=0;
		}
		if(differenceDoigt<-1){
			camera.rotation.y-=0.1;
			differenceDoigt=0;
		}
		
		//on bouge vers la prochaine étape
		if(deplacementVersEtape!="" && deplacementVersEtape!="etapeAtteinte"){
			//rotation de la caméra vers le cube, voir http://www.gamefromscratch.com/post/2012/11/18/GameDev-math-recipes-Rotating-to-face-a-point.aspx
			var angle = Math.atan2(cubeEtapes.position.x-camera.position.x, cubeEtapes.position.z-camera.position.z );
			// document.getElementById("infos").innerHTML = camera.rotation.y+", il faut aller vers "+angle;
			// on descend doucement la caméra vers le cube
				if(camera.rotation.x<-0){
					camera.rotation.x+=vitesseDeplacementsAuto/24;
					deplacementVersEtape="descendRegardVersCube";
				}			

			if(arrondi(camera.rotation.y)==arrondi(angle)){
				//on prend le cube pour cible si notre rotation est égale
				camera.setTarget(cubeEtapes.position);
				deplacementVersEtape="finRotationVersCube";
			}
			if(arrondi(camera.rotation.y)>arrondi(angle) && deplacementVersEtape!="TourneSensAntiHoraire"){
				//on tourne vers le cube si notre rotation est supérieure
				camera.rotation.y-=vitesseDeplacementsAuto/6;
				deplacementVersEtape="TourneSensHoraire";
			}
			if(arrondi(camera.rotation.y)<arrondi(angle) && deplacementVersEtape!="TourneSensHoraire"){
				//on tourne vers le cube si notre rotation est supérieure
				camera.rotation.y+=vitesseDeplacementsAuto/6;
				deplacementVersEtape="TourneSensAntiHoraire";
			}
			//déplacement de la caméra vers le cube
			if(arrondi(camera.position.x)!=(cubeEtapes.position.x) || arrondi(camera.position.z)!=arrondi(cubeEtapes.position.z)){
				if(deplacementVersEtape=="finRotationVersCube"){
					var posXCam = Math.sin(camera.rotation.y);
					var posZCam = Math.cos(camera.rotation.y);
					camera.position.x += posXCam*vitesseDeplacementsAuto;
					camera.position.z += posZCam*vitesseDeplacementsAuto;			
				}
			}
			// document.getElementById("infos").innerHTML = ""+arrondi(camera.position.x)+","+arrondi(camera.position.z)+" / "+(cubeEtapes.position.x)+","+arrondi(cubeEtapes.position.z);
			if(arrondi(camera.position.x)==(cubeEtapes.position.x) && arrondi(camera.position.z)==arrondi(cubeEtapes.position.z)){
				//on est arrivé, fin des déplacements
				numeroEtape=numeroEtape+1;
				deplacementVersEtape="etapeAtteinte";
				// alert("fin");
			}
		}
		
		//action spéciale aux étapes
		if(deplacementVersEtape=="etapeAtteinte"){
			if(numeroEtape==1){
				//DEVANT LE BANC
				//on tourne lentement pour voir les delaunay
				if(camera.rotation.y>-0.7){
					camera.rotation.y-=vitesseDeplacementsAuto/6;
				}
				else if(camera.rotation.y>-0.9){
					camera.rotation.y-=vitesseDeplacementsAuto/10;
				}
				else if(camera.rotation.y>-1.1){
					camera.rotation.y-=vitesseDeplacementsAuto/20;
				}
				else if(camera.rotation.y>-1.2){
					deplacementVersEtape="";
					// alert("fin de etape 1");
				}
				
				//le cube va devant les Picassos
				cubeEtapes.position=new BABYLON.Vector3(etapes[1][0],etapes[1][1],etapes[1][2]);
			}
			if(numeroEtape==2){
				//DEVANT LES ESQUISSES DE PICASSOS
				//on tourne lentement pour voir les tableaux
				if(camera.rotation.y<1.3){
					camera.rotation.y+=vitesseDeplacementsAuto/6;
				}
				else if(camera.rotation.y<1.4){
					camera.rotation.y+=vitesseDeplacementsAuto/10;
				}
				else if(camera.rotation.y<1.546){
					camera.rotation.y+=vitesseDeplacementsAuto/20;
				}
				else if(camera.rotation.y>1.546){
					deplacementVersEtape="";
					// alert("fin de etape 2");
				}
				//le cube va devant le Metzinger
				cubeEtapes.position=new BABYLON.Vector3(etapes[2][0],etapes[2][1],etapes[2][2]);				
			}
			if(numeroEtape==3){
				//DEVANT LE METZINGER
				//on tourne lentement pour voir les tableaux
				if(camera.rotation.y<1.3){
					camera.rotation.y+=vitesseDeplacementsAuto/6;
				}
				else if(camera.rotation.y<1.4){
					camera.rotation.y+=vitesseDeplacementsAuto/10;
				}
				else if(camera.rotation.y<1.546){
					camera.rotation.y+=vitesseDeplacementsAuto/20;
				}
				else if(camera.rotation.y>1.546){
					deplacementVersEtape="";
					// alert("fin de etape 2");
				}
				//le cube va devant le Durain
				cubeEtapes.position=new BABYLON.Vector3(etapes[numeroEtape][0],etapes[numeroEtape][1],etapes[numeroEtape][2]);				
			}
			if(numeroEtape==4){
				//DEVANT LA NATURE MORTE DURAIN
				//on tourne lentement pour voir les tableaux
				if(camera.rotation.y<1){
					camera.rotation.y+=vitesseDeplacementsAuto/6;
				}
				else if(camera.rotation.y<1.1){
					camera.rotation.y+=vitesseDeplacementsAuto/10;
				}
				else if(camera.rotation.y<1.2){
					camera.rotation.y+=vitesseDeplacementsAuto/20;
				}
				else if(camera.rotation.y>1.2){
					deplacementVersEtape="";
					// alert("fin de etape 2");
				}
				//le cube va devant le Herbin
				cubeEtapes.position=new BABYLON.Vector3(etapes[numeroEtape][0],etapes[numeroEtape][1],etapes[numeroEtape][2]);				
			}
			if(numeroEtape==5){
				//DEVANT LA NATURE MORTE DURAIN
				//on tourne lentement pour voir les tableaux
				if(camera.rotation.y<1.3){
					camera.rotation.y+=vitesseDeplacementsAuto/6;
				}
				else if(camera.rotation.y<1.4){
					camera.rotation.y+=vitesseDeplacementsAuto/10;
				}
				else if(camera.rotation.y<1.56){
					camera.rotation.y+=vitesseDeplacementsAuto/20;
				}
				else if(camera.rotation.y>1.2){
					deplacementVersEtape="";
				}
				//le cube va devant le Herbin
				cubeEtapes.position=new BABYLON.Vector3(etapes[numeroEtape][0],etapes[numeroEtape][1],etapes[numeroEtape][2]);				
			}
			if(numeroEtape==6){
				//DEVANT OTTO
				//on tourne lentement pour voir les tableaux
				if(camera.rotation.y<-0.2){
					camera.rotation.y+=vitesseDeplacementsAuto/6;
				}
				else if(camera.rotation.y<-0.1){
					camera.rotation.y+=vitesseDeplacementsAuto/10;
				}
				else if(camera.rotation.y<0){
					camera.rotation.y+=vitesseDeplacementsAuto/20;
				}
				else if(camera.rotation.y>0){
					deplacementVersEtape="";
				}
				//le cube va devant la statue
				cubeEtapes.position=new BABYLON.Vector3(etapes[numeroEtape][0],etapes[numeroEtape][1],etapes[numeroEtape][2]);				
			}
			if(numeroEtape==7){
				//DEVANT STATUE
				//on tourne lentement pour voir les tableaux
				if(camera.rotation.y>-2.5){
					camera.rotation.y-=vitesseDeplacementsAuto/6;
				}
				else if(camera.rotation.y>-2.7){
					camera.rotation.y-=vitesseDeplacementsAuto/10;
				}
				else if(camera.rotation.y>-2.98){
					camera.rotation.y-=vitesseDeplacementsAuto/20;
				}
				else if(camera.rotation.y<-2.98){
					deplacementVersEtape="";
				}
				//le cube va devant le delaunay rythme2
				cubeEtapes.position=new BABYLON.Vector3(etapes[numeroEtape][0],etapes[numeroEtape][1],etapes[numeroEtape][2]);				
			}
			if(numeroEtape==8){
				// alert("fin de la demo");		
			}
		}
		if(metzingerAChanger==1){
			ombreMetzinger.visibility=0;
			if(metzingerLOiseauBleu.position.x>23){
				metzingerLOiseauBleu.position.x-=0.10;
				document.getElementById("infos").innerHTML = ""+metzingerLOiseauBleu.position.x;
			}
			if(metzingerLOiseauBleu.position.x<23){
				metzingerLOiseauBleu.position.y-=0.20;
			}
			if(metzingerLOiseauBleu.position.y<-2){
				delaunayEffeil.position.y+=0.10;
				document.getElementById("infos").innerHTML = ""+delaunayEffeil.position.y;
			}
			if(delaunayEffeil.position.y>3.8 && metzingerLOiseauBleu.position.y<-2){
				metzingerAChanger=0;
				document.getElementById("imageZoomOeuvre").style.display="block";
			}
		}
		if(pilier1){
			// pilier1.position.y-=0.08;
			// pilier1.rotation.y-=0.005;
			// pilier2.position.y-=0.08;
			// pilier2.rotation.y-=0.005;
			// pilier3.position.y-=0.08;
			// pilier3.rotation.y-=0.005;
			// pilier4.position.y-=0.08;
			// pilier4.rotation.y-=0.005;
		}
	});	
	
    // Lancement de la boucle principale
    engine.runRenderLoop(function() {
		// document.getElementById("infos").innerHTML = hdr.getCurrentLuminance()+'/'+hdr.getOutputLuminance();
		// document.getElementById("infos").innerHTML = hdr.getOutputLuminance();
		// hdr.update();
		// document.getElementById("infos").innerHTML = ""+camera.position.x+","+camera.position.y+","+camera.position.z+" rot:"+camera.rotation;

		// document.getElementById("infos").innerHTML = "x:"+spotMisEnPlace.position+" rot:"+spotMisEnPlace.direction;
        scene.render();
		document.getElementById("fps").innerHTML = engine.getFps().toFixed() + " fps";
     });
	 
	 
	//When click event is raised
	window.addEventListener("click", function () {
	   // We try to pick an object
	   var pickResult = scene.pick(scene.pointerX, scene.pointerY);
	   // alert(pickResult.pickedMesh.name+" "+pickResult.distance);
			 
		if(pickResult.distance<5){
			// for(var name in pickResult) {
				// alert(name);
				// var value = pickResult[name];
				// alert(value);
			// }
				// alert(pickResult.pickedMesh.name);
			// if(pickResult.pickedMesh.name=="derainNatureMorteATable"){
				// derainPort.position=new BABYLON.Vector3(20.9303,2.8407,2.4946)
				// derainNatureMorteATable.position.y=30;
			// }
			// if(pickResult.pickedMesh.name=="derainNatureMorteATable"){
				// derainPort.position=new BABYLON.Vector3(20.9303,2.8407,2.4946)
				// derainNatureMorteATable.position.y=30;
			// }
			if(document.getElementById("presentationOeuvre").style.display!="block"){				
				//on dézoome par défaut et on vire l'imahe
				document.getElementById("imageZoomOeuvre").style.maxWidth = "95%";
				document.getElementById("imageZoomOeuvre").style.maxHeight = "95%";
				document.getElementById('imageZoomOeuvre').src="";
				document.getElementById("boutonChanger").style.display="none";
				if(pickResult.pickedMesh.name=="picassoArlequin"){
					document.getElementById("textePresentationOeuvre").innerHTML="<b>Pablo PICASSO</b><br><br>Two acrobats with a dog<br>1905<br>Lithographer Jacques VILLON<br><hr>Lithography in colors<br>88,3 x 65,9 cm";
					document.getElementById("presentationOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").scrollTop = 0;
					document.getElementById('imageZoomOeuvre').src="img/tableaux/picassoArlequinEnfantEtChien.jpg";
					document.getElementById("boutonsInterfaceTableaux").style.display="block";
				}
				if(pickResult.pickedMesh.name=="picassoBusteDHomme"){
					document.getElementById("textePresentationOeuvre").innerHTML="<b>Pablo PICASSO</b><br><br>Buste d'homme<br>Février 1905<hr>Pointe sèche<br>51 x 33 cm (hors marge)";
					document.getElementById("presentationOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").style.display="block";
					document.getElementById('imageZoomOeuvre').src="img/tableaux/picassoBusteDHomme.jpg";
					document.getElementById("zoomOeuvre").scrollTop = 0;
					document.getElementById("boutonsInterfaceTableaux").style.display="block";
				}
				if(pickResult.pickedMesh.name=="picassoAuCirque"){
					document.getElementById("textePresentationOeuvre").innerHTML="<b>Pablo PICASSO</b><br><br>Au cirque<br>Titre attribué : Deux danseurs sur un cheval<br>1905<hr>Gravure datée 1905 mais imprimée en 1913<br>Pointe sèche<br>51 x 33 cm (hors marge)";
					document.getElementById("presentationOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").scrollTop = 0;
					document.getElementById('imageZoomOeuvre').src="img/tableaux/picassoAuCirque.jpg";
					document.getElementById("boutonsInterfaceTableaux").style.display="block";
				}
				if(pickResult.pickedMesh.name=="picassoSaltimbanque"){
					document.getElementById("textePresentationOeuvre").innerHTML="<b>Pablo PICASSO</b><br><br>Saltimbanque au repos<br>1905<hr>Pointe sèche en couleurs<br>51 x 33 cm (hors marge)";
					document.getElementById("presentationOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").style.display="block";
					document.getElementById('imageZoomOeuvre').src="img/tableaux/picassoSaltimbanqueAuRepos.jpg";
					document.getElementById("zoomOeuvre").scrollTop = 0;
					document.getElementById("boutonsInterfaceTableaux").style.display="block";
				}
				if(pickResult.pickedMesh.name=="picassoLaToilette"){
					document.getElementById("textePresentationOeuvre").innerHTML="<b>Pablo PICASSO</b><br><br>La toilette de la mère<br>Gravure datée 1905 mais imprimée en 1913<hr>Pointe sèche en couleurs<bR>Eau-forte sur zinc<br>51 x 33 cm (hors marge)";
					document.getElementById("presentationOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").style.display="block";
					document.getElementById('imageZoomOeuvre').src="img/tableaux/picassoLaToiletteDeLaMere.jpg";
					document.getElementById("zoomOeuvre").scrollTop = 0;
					document.getElementById("boutonsInterfaceTableaux").style.display="block";
				}
				if(pickResult.pickedMesh.name=="vanDongenCorbeille"){
					document.getElementById("textePresentationOeuvre").innerHTML="<b>Kees van DONGEN</b><br><br>Nu à la corbeille de fleurs<br>Vers 1908<hr>Huile sur carton marouflé sur toile<br>46 x 42 cm";
					document.getElementById("presentationOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").scrollTop = 0;
					document.getElementById('imageZoomOeuvre').src="img/tableaux/vanDongenCorbeille.jpg";
					document.getElementById("boutonsInterfaceTableaux").style.display="block";
				}
				if(pickResult.pickedMesh.name=="metzingerLOiseauBleu"){
					document.getElementById("textePresentationOeuvre").innerHTML="<b>Jean METZINGER</b><br><br>L'oiseau bleu<br>1912 - 1913<br><hr>Huile sur toile<br>230 x 196 cm";
					document.getElementById("presentationOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").scrollTop = 0;
					document.getElementById('imageZoomOeuvre').src="img/tableaux/metzingerLOiseauBleu.jpg";
					document.getElementById("boutonsInterfaceTableaux").style.display="block";
					document.getElementById("boutonChanger").style.display="block";
				}
				if(pickResult.pickedMesh.name=="derainNatureMorteATable"){
					document.getElementById("textePresentationOeuvre").innerHTML="<b>André DERAIN</b><br><br>Nature morte à la table<br>1910<br><hr>Huile sur toile<br>92 x 71 cm";
					document.getElementById("presentationOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").scrollTop = 0;
					document.getElementById('imageZoomOeuvre').src="img/tableaux/derainNatureMorte.jpg";
					document.getElementById("boutonsInterfaceTableaux").style.display="block";
				}
				if(pickResult.pickedMesh.name=="herbinNatureMorte"){
					document.getElementById("textePresentationOeuvre").innerHTML="<b>Auguste HERBIN</b><br><br>Nature morte<br>1913<br><hr>Huile sur toile<br>116 x 73 cm";
					document.getElementById("presentationOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").scrollTop = 0;
					document.getElementById('imageZoomOeuvre').src="img/tableaux/herbinNatureMorte.jpg";
					document.getElementById("boutonsInterfaceTableaux").style.display="block";
				}
				if(pickResult.pickedMesh.name=="ottoStolp"){
					document.getElementById("textePresentationOeuvre").innerHTML="<b>Georges BRAQUE</b><br><br>Tête de femme<br>1909<br><hr>Huile sur toile<br>41 x 33 cm";
					document.getElementById("presentationOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").scrollTop = 0;
					document.getElementById('imageZoomOeuvre').src="img/tableaux/braqueTeteDeFemme.jpg";
					document.getElementById("boutonsInterfaceTableaux").style.display="block";
				}
				if(pickResult.pickedMesh.name=="gleizesBaigneuses"){
					document.getElementById("textePresentationOeuvre").innerHTML="<b>Albert GLEIZES</b><br><br>Les baigneuses<br>1912<br><hr>Huile sur toile<br>105 x 171 cm";
					document.getElementById("presentationOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").scrollTop = 0;
					document.getElementById('imageZoomOeuvre').src="img/tableaux/gleizesBaigneuses.jpg";
					document.getElementById("boutonsInterfaceTableaux").style.display="block";
				}
				if(pickResult.pickedMesh.name=="delaunayEquipeCardiff"){
					document.getElementById("textePresentationOeuvre").innerHTML="<b>Robert DELAUNAY</b><br><br>L'équipe de Cardiff<br>1912 - 1913<br><hr>Huile sur toile<br>326 x 208 cm";
					document.getElementById("presentationOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").scrollTop = 0;
					document.getElementById('imageZoomOeuvre').src="img/tableaux/delaunayEquipeCardiff.jpg";
					document.getElementById("boutonsInterfaceTableaux").style.display="block";
				}
				if(pickResult.pickedMesh.name=="delaunayEffeil"){
					document.getElementById("textePresentationOeuvre").innerHTML="<b>Robert DELAUNAY</b><br><br>Tour Effeil<br>1926<br><hr>Huile sur toile<br>169 x 86 cm";
					document.getElementById("presentationOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").style.display="block";
					document.getElementById("zoomOeuvre").scrollTop = 0;
					document.getElementById('imageZoomOeuvre').src="img/tableaux/tourEffeil.jpg";
					document.getElementById("boutonsInterfaceTableaux").style.display="block";
				}
			}
		}
	});
	
	sliderLumiere.addEventListener("input", function () {
		light2.direction.y=sliderLumiere.value/100;
		if((Math.abs(2.6+light2.direction.y)/2)<1){
			hemiLumiere.intensity = Math.abs(2.6+light2.direction.y)/2;
		}
	});
	// sliderspotMisEnPlacePosX.addEventListener("change", function () {
		// spotMisEnPlace.position.x=sliderspotMisEnPlacePosX.value;
	// });
	// sliderspotMisEnPlacex.addEventListener("change", function () {
		// spotMisEnPlace.direction.x=sliderspotMisEnPlacex.value;
	// });
	// sliderspotMisEnPlacey.addEventListener("change", function () {
		// spotMisEnPlace.direction.y=sliderspotMisEnPlacey.value;
	// });
	// sliderspotMisEnPlacez.addEventListener("change", function () {
		// spotMisEnPlace.direction.z=sliderspotMisEnPlacez.value;
	// });
	// sliderspotMisEnPlacez.addEventListener("change", function () {
		// spotMisEnPlace.position.z=sliderspotMisEnPlacez.value;
	// });
	// sliderspotMisEnPlaceangle.addEventListener("change", function () {
		// spotMisEnPlace.angle=sliderspotMisEnPlaceangle.value;
	// });
	// sliderspotMisEnPlaceDirY.addEventListener("change", function () {
		// spotMetzinger.direction.y=sliderspotMisEnPlaceDirY.value;
	// });
	spotMetzingerAngle.addEventListener("change", function () {
		spotMetzinger.angle=spotMetzingerAngle.value;
	});
	
	canvas.addEventListener('touchend', function(){oldDoigtX=0;}, true);
	canvas.addEventListener('touchstart', function(){oldDoigtX=0;}, true);
canvas.addEventListener('mousemove', bougeDoigt, false);
canvas.addEventListener('touchmove', bougeDoigt, false); //POUR DEMO TACTILE
	
	
	sliderCouleurSpotOttoStolp.addEventListener("change", function () {
		spot2CouleurR=SpotOttoStolpCouleurR.value;
		spotOttoStolp.diffuse = new BABYLON.Color3(spot2CouleurR, 0.77, 0.77);
	});
	
	
}

function bougeDoigt(e){
	// var doigtX=  e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
	var doigtX=e.targetTouches[0].pageX;
	// doigtY=e.targetTouches[0].pageY;
		if(oldDoigtX>doigtX && oldDoigtX>0){
			// alert(oldDoigtX+"/"+doigtX);
		}
		// alert(doigtX);
	differenceDoigt=oldDoigtX-doigtX;
	document.getElementById("infos").innerHTML = differenceDoigt ;
	oldDoigtX=doigtX;
}

function arrondi(nombre){
	return Math.round(nombre*10)/10;
}

function avancerViaBouton() {
	if(deplacementVersEtape==""){
		deplacementVersEtape="debutDeplacement";
		fermerFenetre();
	}
}

function fermerFenetre(){
	// document.getElementById("presentationOeuvre").style.display="none";
	// document.getElementById("zoomOeuvre").style.display="none";
	// document.getElementById("boutonsInterfaceTableaux").style.display="none";
	// opacitePresentationOeuvre=0;
	opaciteAReduire=1;
}
function zoomerDezoomerOeuvre(){
	if(document.getElementById("imageZoomOeuvre").style.maxWidth != "95%"){
	 document.getElementById("imageZoomOeuvre").style.maxWidth = "95%";
	 document.getElementById("imageZoomOeuvre").style.maxHeight = "95%";
	}else{
		document.getElementById("imageZoomOeuvre").style.maxWidth = "";
		document.getElementById("imageZoomOeuvre").style.maxHeight = "";
	}
}
function afficherInterfaceChangerTableau(){
	document.getElementById("iframeRechercheTableau").style.display="block";
	document.getElementById("imageZoomOeuvre").style.display="none";
}

function changerTableau(){
	fermerFenetre();
	document.getElementById("iframeRechercheTableau").style.display="none";		
	metzingerAChanger=1;
}