# 3D MUSEUM powered by BABYLON.JS library

A 2015 project.

![](demo.mp4)

## Live demo:
http://s140685957.onlinehome.fr/museum/museumtest3latest/

## FEATURES
* Walk around a 3D museum.
* Auto-walk.
* Get a closer view of a painting and a description when clicking on it.
* Change the lighting of the room.
* Switch one painting to another using the switch button and searching for "tower effeil del" 

